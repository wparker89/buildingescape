// Fill out your copyright notice in the Description page of Project Settings.

#include "BuildingEscape.h"
#include "OpenDoor.h"


// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	// ...
    
    auto owner = GetOwner();
    
    auto rotator = FRotator(0.0f, 60.0f, 0.0f);
    
    owner->SetActorRotation(rotator);
    
    
    auto ownerName = owner->GetName();
    auto objectRotation = owner->GetActorRotation().ToString();
    UE_LOG(LogTemp, Warning, TEXT("%s is %s."), *ownerName, *objectRotation);
	
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

